/*
 * usart.h
 *
 *  Created on: 23 Jul 2016
 *      Author: Boyang
 */

#ifndef APPLICATION_USER_USART_H_
#define APPLICATION_USER_USART_H_

typedef struct{

	unsigned char *buffer;
	unsigned char indx;
	unsigned char length;

}SLIPBufferHandler;

void initSLIPBufferHandler(SLIPBufferHandler *b, unsigned char *buffer);
char decodeSLIP(SLIPBufferHandler *b, unsigned char newChar);
void clearSLIPBuffer(SLIPBufferHandler *b , unsigned char *tempBuffer);

#endif /* APPLICATION_USER_USART_H_ */
