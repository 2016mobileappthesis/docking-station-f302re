/*
 * usart.c
 *
 *  Created on: 23 Jul 2016
 *      Author: Boyang
 */
#include "usart.h"
#include "defines.h"

void initSLIPBufferHandler(SLIPBufferHandler *b, unsigned char *buffer){
			b->buffer = buffer;
			b->indx = 0;
			b->length = 0;
}

void clearSLIPBuffer(SLIPBufferHandler *b , unsigned char *tempBuffer){

	int i;
	unsigned char length;
	unsigned char *tempbuffer = b->buffer;

	length = b->length;
	for(i = 0;i<length;i++){
		tempBuffer[i] = tempbuffer[i];
		tempbuffer[i]= 0;
	};

	b->length = 0;
}

char decodeSLIP(SLIPBufferHandler *b, unsigned char newChar){

	unsigned char _indx;
	unsigned char *tempbuffer = b->buffer;

	_indx = b->indx;

	if(newChar == SLIP_END){
		b->indx = 0;			//reset the pointer
		b->length =_indx+1;
		return 0; 				//if the function returns 0 then buffer contains
								//a packet
	}
	//if newChar is == 0xDC
	else if(newChar == SLIP_ESC_END){
		//if not first Byte
		if(_indx != 0){

			if(tempbuffer[_indx-1] == SLIP_ESC){  	//if the previous byte is 0xDB
				tempbuffer[_indx-1] = SLIP_END; 	//replace previous byte with 0xC0
			}
			else{
				tempbuffer[_indx] = newChar;		//nothing special. Add new char to buffer and increment index.
				b-> indx++;
			}
		}
		//first byte
		else {
			tempbuffer[_indx] = newChar;
			b->indx++;
		}
	return 1;
	}
	//if newChar is == 0xDD
	else if(newChar == SLIP_ESC_ESC){
		//if not first Byte
		if(_indx != 0){

			if(tempbuffer[_indx-1] == SLIP_ESC){  //if the previous Byte is 0xDB
				tempbuffer[_indx - 1] = SLIP_ESC; //replace previous Byte with 0xDB
			}
			else{
				tempbuffer[_indx] = newChar;	  //else nothing special, store it and increment indx
				b->indx++;
			}
		}
		//first Byte
		else{
			tempbuffer[_indx] = newChar;
				b->indx++;
		}
	return 1;
	}

	//no special character received
	else{
		tempbuffer[_indx] = newChar;
		b -> indx++;
	}
	return 1;
}

