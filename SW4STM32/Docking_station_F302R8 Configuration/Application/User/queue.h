/**
  ******************************************************************************
  * <h2><center>&copy; COPYRIGHT 2014 Rheem Australia Pty Ltd</center></h2>
  ******************************************************************************
  */

#ifndef APPLICATION_USER_QUEUE_H_
#define APPLICATION_USER_QUEUE_H_
typedef struct {
	unsigned short length;
	unsigned char * buffer;
	unsigned short inIdx;
	unsigned short outIdx;
} CharFIFO;

#ifndef FALSE
  #define FALSE 0
  #define TRUE !FALSE
#endif

#ifndef null
	#define null ((void *)0)
#endif

void InitFIFO(CharFIFO *q, unsigned char *buffer, unsigned short len);
char EnQueueFIFO(CharFIFO *q, unsigned char c);
char DeQueueFIFO1(CharFIFO *q, unsigned char *result);
unsigned short DeQueueFIFO(CharFIFO *q, unsigned char *buf, unsigned short len);
#endif /* QUEUE_H_ */
