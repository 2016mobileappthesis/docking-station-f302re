/**
  ******************************************************************************
  */

#include "queue.h"

/**
  * @brief  This function initialises a FIFO queue, storage must be provided in the param
  * @param  q: The queue to be initialised.
  * @param  buffer: valid and initialised buffer storage.
  *    This parameter must be initialised with storage space allocated before passed in.
  * @param  len: length of buffer in bytes.
  * @retval None
  */
void InitFIFO(CharFIFO *q, unsigned char *buffer, unsigned short len)
{
	if (q != null)
	{
		q->buffer = buffer;
		q->inIdx = 0;
		q->outIdx = 0;
		q->length = len;
	}
}

/**
  * @brief  This function queues a char at the back of the queue
  * @param  q: The target queue.
  * @param  c: char to be queued
  * @retval  0 when there is no queue
  * 		 1 when c is queued
  * 		-1 when queuing will cause data loss, nothing is modified
  */
char EnQueueFIFO(CharFIFO *q, unsigned char c)
{
	char _inIdx;

	if (q == null)
		return 0;						// cannot queue if there is no queue
	_inIdx = q->inIdx + 1;
	_inIdx %= q->length;				// next position if queued
	if(_inIdx == q->outIdx)				// check for overflow
		return -1;						// we cannot add here, and let the caller know

	*(q->buffer + q->inIdx) = c;		// queue when we can
	q->inIdx++;							// ready for next
	q->inIdx %= q->length;
	return 1;							// so we queued 1
}

/**
  * @brief  This function retrieves bytes from the front of the queue to a buffer
  * @param  q: Target queue.
  * @param  buf: pointer to the result
  * @param  len: byte count to dequeue
  * @retval Number of bytes retrieved
  */
unsigned short DeQueueFIFO(CharFIFO *q, unsigned char *buf, unsigned short len)
{
	if (q == null)
		return 0;						// cannot dequeue if there is no queue
	if (q->inIdx == q->outIdx)			// check if we have any
		return 0;						// tell people we do not have
	unsigned short i = 0;
	while (q->inIdx != q->outIdx && i < len)
	{
       // DebugPin_Toggling(); not lockup main loop.
		*(buf + i) = *(q->buffer + q->outIdx);	// dequeue when we can
		q->outIdx++;						// ready for next
		q->outIdx %= q->length;
		i++;
	}
	return i;							// so we dequeued i
}


/**
  * @brief  This function retrieves the char from the front of the queue
  * @param  q: Target queue
  * @param  result: pointer to the result
  * @retval 0 when no data can be retrieved, 1 when data has been placed in result
  */
char DeQueueFIFO1(CharFIFO *q, unsigned char *result)
{
	return DeQueueFIFO(q, result, 1);

	/*
	if (q == null)
		return 0;						// cannot dequeue if there is no queue
	if (q->inIdx == q->outIdx)			// check if we have any
		return 0;						// tell people we do not have
	*result = *(q->buffer + q->outIdx);	// dequeue when we can
	q->outIdx++;						// ready for next
	q->outIdx %= q->length;
	return 1;							// so we dequeued 1
	*/
}


