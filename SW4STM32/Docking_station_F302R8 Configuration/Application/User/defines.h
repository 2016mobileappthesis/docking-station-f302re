/*
 * defines.h
 *
 *  Created on: 30 Jul 2016
 *      Author: Boyang
 */

#ifndef APPLICATION_USER_DEFINES_H_
#define APPLICATION_USER_DEFINES_H_

#define UART_RX_BUFFER_LEN 256

#define SLIP_END 0xC0
#define SLIP_ESC 0xDB
#define SLIP_ESC_END 0xDC
#define SLIP_ESC_ESC 0xDD


typedef enum{
	Motor1,
	Motor2
}MotorType;

typedef void(*InitServoPtr)(void);

#endif /* APPLICATION_USER_DEFINES_H_ */
